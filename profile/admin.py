from django.contrib import admin
from django.contrib.gis.admin import OSMGeoAdmin

from profile.models import Profile, Photo


@admin.register(Profile)
class ProfileAdmin(OSMGeoAdmin):
    list_display = ('id', 'user',)


@admin.register(Photo)
class ProfileAdmin(admin.ModelAdmin):
    list_display = ('id', 'profile', 'photo')

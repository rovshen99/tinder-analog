from enum import Enum

from django.contrib.auth.models import User
from django.contrib.gis.geos import Point
from django.db import models
from django.contrib.gis.db import models as gismodels
from django.db.models.signals import post_save
from django.dispatch import receiver


class TimeStampMixin(models.Model):
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    class Meta:
        abstract = True


class SubscriptionChoices(Enum):
    ST = 'Standard'
    VIP = 'VIP'
    PRE = 'Premium'


class Profile(TimeStampMixin):
    user = models.OneToOneField(User, on_delete=models.CASCADE, related_name='profile')
    description = models.TextField(null=True, blank=True)
    subscription = models.CharField(max_length=3, default='St',
                                    choices=[(tag.name, tag.value) for tag in SubscriptionChoices])
    birthday = models.DateField(blank=True, null=True)
    liked_profiles = models.ManyToManyField('self', symmetrical=False, related_name='likes', null=True, blank=True)
    disliked_profiles = models.ManyToManyField('self', symmetrical=False, related_name='dislikes', null=True, blank=True)
    swipe_limit = models.PositiveSmallIntegerField(null=True, blank=True)
    used_swipes_count = models.PositiveSmallIntegerField(null=True, blank=True, default=0)
    last_update_swipes = models.DateTimeField(auto_now_add=True)
    location = gismodels.PointField(default=Point(0, 0), blank=True)
    last_location_update = models.DateTimeField(auto_now_add=True)
    search_radius = models.PositiveIntegerField(default=10)

    class Meta:
        ordering = ("-id",)
        verbose_name = 'profile'
        verbose_name_plural = 'profiles'

    def __str__(self):
        return self.user.username

    def save(self, *args, **kwargs):
        if self.subscription == 'St':
            self.swipe_limit = 20
            self.search_radius = 10
        elif self.subscription == 'VIP':
            self.swipe_limit = 100
            self.search_radius = 25
        elif self.subscription == 'Pre':
            self.swipe_limit = None
        super(Profile, self).save(*args, **kwargs)


@receiver(post_save, sender=User)
def create_or_update_user_profile(sender, instance, created, **kwargs):
    if created:
        Profile.objects.create(user=instance)
    instance.profile.save()


class Photo(TimeStampMixin):
    profile = models.ForeignKey(Profile, on_delete=models.CASCADE, related_name='photos')
    photo = models.ImageField(upload_to='media/photos')

from datetime import datetime, timezone

from django.contrib.gis.geos import Point
from django.shortcuts import get_object_or_404

from geopy.distance import geodesic

from profile.models import Profile


def has_swipe(profile_id):
    profile = get_object_or_404(Profile, pk=profile_id)
    return (profile.swipe_limit is None or profile.swipe_limit > 0) and \
           (profile.swipe_limit > profile.used_swipes_count)


def update_swipes(profile_id):
    profile = get_object_or_404(Profile, pk=profile_id)
    current_time = datetime.now(timezone.utc)
    if 3600 * 24 < (current_time - profile.last_update_swipes).seconds:
        profile.last_update_swipes = current_time
        profile.save()


def can_update_location(profile):
    current_time = datetime.now(timezone.utc)
    if 3600 * 2 < (current_time - profile.last_location_update).seconds:
        return True
    return False


def change_location(profile_id, long, lat):
    profile = get_object_or_404(Profile, pk=profile_id)
    profile.last_location_update = datetime.now(timezone.utc)
    profile.location = Point(long, lat)


def get_profiles_for_swipe(profile_id):
    profile = get_object_or_404(Profile, pk=profile_id)
    return Profile.objects.exclude(likes=profile) \
        .exclude(dislikes=profile).exclude(id=profile_id)


def get_distance_difference(profile_id, other_profile_id):
    current_user_profile = get_object_or_404(Profile, pk=profile_id)
    other_profile = get_object_or_404(Profile, pk=other_profile_id)
    return geodesic(current_user_profile.location, other_profile.location).kilometers


def is_profile_in_radius(profile_id, other_profile_id):
    current_user_profile = get_object_or_404(Profile, pk=profile_id)
    other_profile = get_object_or_404(Profile, pk=other_profile_id)
    return get_distance_difference(current_user_profile.id, other_profile.id) <= current_user_profile.search_radius


def add_like_or_dislike(profile_id, other_profile_id, like=True):
    current_user_profile = get_object_or_404(Profile, pk=profile_id)
    other_profile = get_object_or_404(Profile, pk=other_profile_id)
    if like:
        current_user_profile.liked_profiles.add(other_profile)
    else:
        current_user_profile.disliked_profiles.add(other_profile)
    current_user_profile.save()

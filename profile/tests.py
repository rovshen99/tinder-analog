import pytest
from django.contrib.auth.models import User
from django.db.models import signals
from mixer.backend.django import mixer

from profile.models import Profile

pytestmark = pytest.mark.django_db


class TestProfileModel:

    def test_str_return(self):
        signals.post_save.receivers = []
        user = mixer.blend(User, username='Tom')
        mixer.blend(Profile, user=user)
        profile_result = Profile.objects.last()

        assert str(profile_result) == user.username

    def test_standard_profile(self):
        mixer.blend(Profile, subscription='St')
        profile_result = Profile.objects.last()

        assert profile_result.swipe_limit == 20
        assert profile_result.search_radius == 10

    def test_vip_profile(self):
        mixer.blend(Profile, subscription='VIP')
        profile_result = Profile.objects.last()

        assert profile_result.swipe_limit == 100
        assert profile_result.search_radius == 25

    def test_premium_profile(self):
        mixer.blend(Profile, subscription='PRE')
        profile_result = Profile.objects.last()

        assert profile_result.swipe_limit is None

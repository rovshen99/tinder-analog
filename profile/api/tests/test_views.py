import json
import tempfile
import pytest

from PIL import Image
from mixer.backend.django import mixer

from django.contrib.auth.models import User
from django.test import TestCase
from rest_framework.test import APIClient
from rest_framework.reverse import reverse

from profile.models import Profile

pytestmark = pytest.mark.django_db


class TestViewSets(TestCase):
    def setUp(self):
        self.client = APIClient()
        self.user = User.objects.create_user(username='testUser', password='123456')
        self.profile = mixer.blend(Profile, user=self.user)
        response = self.client.post('/api/token/', {"username": 'testUser', "password": '123456'})
        response_content = json.loads(response.content.decode('utf-8'))
        self.token = response_content["access"]

    def test_add_photo(self):

        image = Image.new('RGB', (100, 100))
        tmp_file = tempfile.NamedTemporaryFile(suffix='.jpg')
        image.save(tmp_file)
        url = '/api/photo/add_photo/'
        with open(tmp_file.name, 'rb') as data:
            print(data)
            response = self.client.post(url, {'photo': data}, HTTP_AUTHORIZATION='Bearer {}'.format(self.token),
                                        format='multipart',)
            assert response.status_code == 201

    def test_add_description(self):
        description = 'Some Test description'
        url = '/api/profile/add_description/'
        response = self.client.patch(url, {'description': description},
                                     HTTP_AUTHORIZATION='Bearer {}'.format(self.token))
        assert response.status_code == 200

    def test_get_matched_profiles(self):
        url = '/api/profile/get-matches/'
        response = self.client.get(url, {}, HTTP_AUTHORIZATION='Bearer {}'.format(self.token))
        print(self.user.profile.description)
        assert response.status_code == 200 or response.status_code == 204

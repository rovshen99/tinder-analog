from rest_framework import viewsets, status
from rest_framework.decorators import action, permission_classes
from rest_framework.response import Response

from profile.api.serializers import PhotoSerializer, ProfileSerializer
from profile.models import Photo, Profile
from profile.permissions import UserIsOwner
from profile.services import has_swipe, get_profiles_for_swipe, is_profile_in_radius, add_like_or_dislike, \
    update_swipes, can_update_location, change_location


class PhotoViewSet(viewsets.ViewSet):
    queryset = Photo.objects.all()

    @action(methods=['POST'], detail=False)
    def add_photo(self, request):
        profile, created = Profile.objects.get_or_create(user=request.user)
        print(profile.id)
        serializer = PhotoSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        serializer.save(profile=profile)
        return Response(serializer.data, status=status.HTTP_201_CREATED)


class ProfileViewSet(viewsets.ViewSet):

    @action(methods=['PATCH'], detail=False, permission_classes=[UserIsOwner])
    def add_description(self, request):
        current_users_profile, created = Profile.objects.get_or_create(user=request.user)
        serializer = ProfileSerializer(current_users_profile, data=request.data, partial=True)
        serializer.is_valid(raise_exception=True)
        serializer.save()
        return Response(serializer.data, status=status.HTTP_200_OK)

    @action(methods=['PATCH'], detail=False, url_path='like-dislike')
    def like_dislike_profile(self, request):
        current_users_profile, created = Profile.objects.get_or_create(user=request.user)

        try:
            other_users_profile = self.request.data['profile']
            other_users_profile = Profile.objects.get(id=int(other_users_profile))
        except Profile.DoesNotExist:
            return Response({"message": "Profile with this id does not exist."}, status=status.HTTP_400_BAD_REQUEST)

        like = int(self.request.data['like'])
        add_like_or_dislike(current_users_profile, other_users_profile, like)
        return Response(status=status.HTTP_200_OK)

    @action(methods=['GET'], detail=False, url_path='swipe-profile')
    def get_profile_for_swipe(self, request):
        current_users_profile = Profile.objects.get(id=self.request.user.profile.id)
        update_swipes(current_users_profile.id)

        if has_swipe(current_users_profile.id):
            other_profiles = get_profiles_for_swipe(current_users_profile.id)
            for other_profile in other_profiles:
                if is_profile_in_radius(current_users_profile.id, other_profile.id):
                    current_users_profile.used_swipes_count += 1
                    current_users_profile.save()
                    serializer = ProfileSerializer(other_profile)
                    return Response(serializer.data, status=status.HTTP_200_OK)
        return Response({"message": "No profile or not left swipes"}, status=status.HTTP_204_NO_CONTENT)

    @action(methods=['GET'], detail=False, url_path='get-matches')
    def get_matched_profiles(self, request):
        current_users_profile = Profile.objects.get(id=self.request.user.profile.id)
        profiles = Profile.objects.filter(likes=current_users_profile).filter(liked_profiles=current_users_profile)
        serializer = ProfileSerializer(profiles, many=True)
        if serializer.data:
            return Response(serializer.data, status=status.HTTP_200_OK)
        return Response({"message": "No matches"}, status=status.HTTP_204_NO_CONTENT)

    @action(methods=['PATCH'], detail=False, url_path='change-location')
    def change_location(self, request):
        current_users_profile = Profile.objects.get(id=self.request.user.profile.id)
        serializer = ProfileSerializer(current_users_profile, data=request.data, partial=True)
        serializer.is_valid(raise_exception=True)
        new_location = serializer.validated_data["location"]
        if can_update_location(current_users_profile):
            change_location(current_users_profile, *new_location)
            return Response({"message": "Success"}, status=status.HTTP_200_OK)
        else:
            return Response({"message": "Location can't be changed in less than 2 hours"},
                            status=status.HTTP_400_BAD_REQUEST)

    @action(methods=['PATCH'], detail=False, url_path='change-radius')
    def change_radius(self, request):
        current_users_profile = Profile.objects.get(id=self.request.user.profile.id)
        serializer = ProfileSerializer(current_users_profile, data=request.data, partial=True)
        serializer.is_valid(raise_exception=True)
        try:
            new_radius = serializer.validated_data["search_radius"]
        except KeyError:
            return Response({"message": " 'search_radius' argument is not valid"},
                            status=status.HTTP_400_BAD_REQUEST)
        if current_users_profile.subscription == 'PRE':
            current_users_profile.search_radius = new_radius
            current_users_profile.save()
            return Response({"message": "Success"}, status=status.HTTP_200_OK)
        else:
            return Response({"message": "Radius can't be changed if profile is not premium"},
                            status=status.HTTP_400_BAD_REQUEST)

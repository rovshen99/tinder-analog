from django.urls import path, include

from rest_framework.routers import DefaultRouter, SimpleRouter, BaseRouter

from profile.api import views

router = DefaultRouter()

router.register('photo', viewset=views.PhotoViewSet, basename='photo')
router.register('profile', viewset=views.ProfileViewSet, basename='profile_view')

urlpatterns = [
    path('api/', include(router.urls))
]

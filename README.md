#Innowise Group - _Tinder Analog_ 
### Docker
It is project very easy to install and deploy in a Docker container.

By default, the Docker will expose port 8000, so change this within the
docker-compose if necessary. When ready, simply use the docker-compose to
build the image. 
!!! You should be in directory project(innowise_test)</div>

    docker-compose up --build
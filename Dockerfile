FROM python:3.8

ENV PYTHONDONTWRITTENBYTECODE 1
ENV PYTHONUNBUFFERED 1

WORKDIR /usr/src/innowise_test

COPY . /usr/src/innowise_test
RUN apt-get update && apt-get install -y git
RUN apt-get install -y gdal-bin libgdal-dev

RUN pip3 install --upgrade pip
RUN python -m venv /venv

ARG CPLUS_INCLUDE_PATH=/usr/include/gdal
ARG C_INCLUDE_PATH=/usr/include/gdal

RUN pip3 install GDAL==$(gdal-config --version)
RUN pip3 install -r requirements_ubuntu.txt
